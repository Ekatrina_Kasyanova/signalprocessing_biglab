### Лабораторная Обработка сигналов
## Формулировка задачи
Пусть на фотографии располагается дверной проем и объект вешалка. Требуется определить пролезет ли объект в дверной проем

## Ограничения, накладываемые на изображение

1. Объект не перекрывают посторонние предметы
2. Освещение - холодный искусственный свет
3. Освещение внутри дверного проема отсутствует
4. Объект может находиться на расстоянии не более 1 метра от проема, где за начало отсчета принимается расположение объекта внутри дверного проема
5. Фотография объекта не может быть сделана снизу вверх
6. Разрешение изображения 4032*3024, 4032*2268. Размер фотографии не превышает 5МБ
7. Формат фотографии .jpg
8. С правой и левой стороны от двери отсутствуют посторонние объекты, имеющие явные вертикальные границы

## План работы

1. Получение имени изображения, загрузка изображения.
2. Измениение изображения: rgb2gray(), gaussian(sigma = 3.4, multichannel = False), adaptiveThreshold()
3. Поиск границ на изображении Canny(sigma = 1)
4. Поиск объекта, как наибольшей связной компоненты 
5. Выделение прямоугольной области, по границе наибольшей связной компоненты
6. Преобразование Хафа для поиска линий, образующих дверной проем: предполагается,что линии вертикальные, отклонены не больше, чем на 0.3 радиана от вертикали, из-за наличника выбираются вторые прямые с каждой стороны
7. Вычисление точек пересечения прямой, проходящей через верхнюю границу прямоугольной области и двух линий, образующих дверь  
8. Если левая и правая границы прямоугольной области находятся между точек, полученых в пункте 7, то объект проходит через дверной проем, иначе - не проходит. 

На исходном dataset'е правильных ответов 80%

## Файлы реппозитория
dataset - Папка, содержащая фотографии для тестирования 
results.txt - Текстовый файл, содержащий правильные ответы для фотографий из dataset
main.py - Исходный код