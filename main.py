from imageio import imread
from math import inf
import numpy as np
import cv2
import os
from imageio import imsave
from skimage.color import rgb2gray
from skimage.feature import canny
from scipy.ndimage.morphology import binary_fill_holes
from skimage.measure import label, regionprops
from skimage.morphology import binary_closing
from skimage import img_as_ubyte
from skimage.filters import gaussian
from skimage.transform import hough_line, hough_line_peaks
from matplotlib import pyplot as plt, cm
from matplotlib.patches import Rectangle


class Line:
    def __init__(self, x):
        self.x = x
        self.y0 = 0
        self.y1 = 0
        self.k = 0
        self.b = 0


def get_largest_component(mask):
    """
    Get largest component
    :param mask: image
    :return: Largest component
    """
    labels = label(mask)
    props = regionprops(labels)
    areas = [prop.area for prop in props]
    largest_comp_id = np.array(areas).argmax()
    return labels == (largest_comp_id + 1)


def find_describing_rectangle(img):
    """
    Find the rectangle surrounding chair
    :param img: image
    :return: left top corner, width and height of rectangle
    """
    prepared_img = binary_fill_holes(binary_closing(img, selem=np.ones((5, 5))))
    result_mask_chair = get_largest_component(prepared_img)

    vertical_indices = np.where(np.any(result_mask_chair, axis=1))[0]
    top, bottom = vertical_indices[0], vertical_indices[-1]

    horizontal_indices = np.where(np.any(result_mask_chair, axis=0))[0]
    left, right = horizontal_indices[0], horizontal_indices[-1]

    corner = (left, top)
    h = bottom - top
    w = right - left
    return corner, w, h


def show_door_rect(img, corner, w, h, left, right):
    """
    Show rectangle rectangle surrounding chair and door lines
    :param img: image
    :param corner: left top corner or rectangle
    :param w: width rectangle
    :param h: height rectangle
    :param left: left door line
    :param right: right door line
    """
    plt.imshow(img, cmap=cm.gray)
    plt.xlim((0, img.shape[1]))
    plt.ylim((img.shape[0], 0))
    plt.gca().add_patch(Rectangle(corner, w, h, linewidth=2, edgecolor='b', facecolor='none'))
    plt.plot((0, img.shape[1]), left, '-b')
    plt.plot((0, img.shape[1]), right, '-b')
    plt.show()


def find_door(matchbox, image, width):
    """
    Find lines describing door
    :param matchbox: transformed image
    :param image:original image
    :param width: width of original image
    :return: left and right lines
    """
    h, theta, d = hough_line(matchbox)
    # plt.imshow(image, cmap=cm.gray)
    min = Line(inf)
    max = Line(-inf)
    min_prev = Line(inf)
    max_prev = Line(-inf)
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d)):
        if abs(angle) < 0.3:
            current = Line(0)
            current.y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
            current.y1 = (dist - image.shape[1] * np.cos(angle)) / np.sin(angle)
            current.k = (current.y0 - current.y1) / (0 - image.shape[1])
            current.b = current.y1 - current.k * image.shape[1]
            current.x = int((width - current.b) / current.k)
            # plt.plot((0, image.shape[1]), (current.y0, current.y1), '-r')
            if current.x < width / 2:
                if min_prev.x < current.x < min.x:
                    min = current
                elif current.x < min_prev.x:
                    min = min_prev
                    min_prev = current
            else:
                if max.x < current.x < max_prev.x:
                    max = current
                elif current.x > max_prev.x:
                    max = max_prev
                    max_prev = current
        # print(min_prev.x, min.x, max.x, max_prev.x)

    # plt.plot((0, image.shape[1]), (min.y0, min.y1), '-b')
    # plt.plot((0, image.shape[1]), (max.y0, max.y1), '-b')
    # plt.xlim((0, image.shape[1]))
    # plt.ylim((image.shape[0], 0))
    # plt.show()
    return min, max


def border_point(y, k, b):
    """
    Solve y = kx + b
    :param y:
    :param k:
    :param b:
    :return:
    """
    if k == 0:
        return y
    else:
        return (y - b) / k


def get_answer(rect_corner, rect_width, left, right):
    """
    If chair goes through the door - Yes, else - No
    :param rect_corner: corner of rectangle surrounding chair
    :param rect_width: width of rectangle surrounding chair
    :param left: left line describing door
    :param right: right line describing door
    :return: Yes or No answer
    """
    answer = 'No'
    left_border = border_point(rect_corner[1], left.k, left.b)
    right_border = border_point(rect_corner[1], right.k, right.b)
    if (rect_width + rect_corner[0]) < max(left_border, right_border) \
            and rect_corner[0] > min(left_border, right_border):
        answer = 'Yes'
    return answer


def intersection_search(image):
    """
    Search is chair goes through the door
    :param image: original image
    :return: answer
    """
    width = image.shape[1]
    if width > 2800:
        h = image.shape[0]
        w = image.shape[1]
        width = 1500
        height = int(width / float(w) * float(h))
        image = cv2.resize(image, (width, height), interpolation=cv2.INTER_AREA)

    gray = rgb2gray(image)
    blur = gaussian(gray, sigma=3.4, multichannel=False)
    adapt = cv2.adaptiveThreshold(img_as_ubyte(blur), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                  cv2.THRESH_BINARY_INV, 13, 2)
    # imsave("adapt.jpg", adapt)

    canny_edge = canny(adapt, 1)
    #    imsave("canny.jpg", img_as_ubyte(canny_edge))

    rect_corner, rect_width, rect_height = find_describing_rectangle(canny_edge)
    left, right = find_door(canny_edge, image, width)

    show_door_rect(image, rect_corner, rect_width, rect_height, (left.y0, left.y1), (right.y0, right.y1))

    return get_answer(rect_corner, rect_width, left, right)


def main():
    root_dir = os.path.abspath("./")
    image_dir = os.path.join(root_dir, "dataset")
    fds_y = os.listdir(image_dir)
    work_res = open('work.txt', 'w')
    correct = 0
    with open('results.txt', 'r') as file:
        rows = (line.rstrip().split(' ') for line in file)
        result_dict = {row[0]: row[1] for row in rows}
    for i in fds_y:
        image = imread(image_dir + '/' + i)
        result = intersection_search(image)
        work_res.write(i + ' ' + result + '\n')
        if result == result_dict[i]:
            correct += 1
    accuracy = correct / 30 * 100
    print(accuracy)


#    image = imread("D:/dataset1/2.jpg")
#    print(intersection_search(image))


if __name__ == '__main__':
    main()
